This solution contains 2 C# files that are compatible with Unity.

To use them in Unity, simply drag PlayerController.cs onto your Player GameObject
and give it the tag "Player", then drag CameraController.cs onto your Camera GameObject

Both files are well commented and should be easy to tweak to your liking.

Should you use these in your Unity projects, I only ask that you source my bitbucket
and keep the comment at the top of each script.