﻿/*-------------------------------------------------*/
/*               PlayerController.cs               */
/*              Created by Ben Silver              */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Whenever this script is added to an object in the scene
   the CharacterController component will be added as well */
[RequireComponent (typeof(CharacterController))]
public class PlayerController : MonoBehaviour
{
    /* Public Variables */
    public Vector3 moveInput;               // Takes the Horizontal and Vertical input from the Player
    public float rotationSpeed = 450.0f;    // Determines how fast the player will rotate
    public float walkSpeed = 5.0f;          // Determines how fast the player will walk
    public float runSpeed = 8.0f;           // Determines how fast the player will run
    public float acceleration = 5.0f;       // Determines how fast the player will accelerate
    public float jumpDist = 5.0f;           // Determines how high the player will jump
    public bool canMove;                    // False if the Player is talking to another character

    /* Private Variables */
    private CharacterController cController;// Reference to the CharacterController component
    private Vector3 currVelocityModifier;   // Used to control the velocity of the GameObject
    private Quaternion targetRotation;      // Used to control the rotation of the GameObject
    private float idleCounter = 0.0f;       // Used with the Animator to determine which idle animation will play
    private bool playerMoving;              // True when the player is moving
    private bool playerRunning;             // True when the player is running
    private static bool playerExists;       // Used to determine if the Player already exists when loading a new level

    // Use this for initialization
    void Start()
    {
        // Referencing the CharacterController component on the GameObject
        cController = GetComponent<CharacterController>();

        // Preventing duplicate players from being made when transferring to other maps
        if (!playerExists)
        {
            // Set playerExists to true
            playerExists = true;

            // GameObject will retain this script when transferred to a new level
            DontDestroyOnLoad(transform.gameObject);
        }
        else
        {
            // If the player already exists the new instance will be destroyed
            Destroy(gameObject);
        }

        // At the start the Player will not be talking with anyone
        canMove = true;
    }
	
	// Update is called once per frame
	void Update()
    {
        // Check if idleCounter is greater than or equal to 6
        if (idleCounter >= 6.0f)
        {
            // If it is greater than or equal to 6 idleCounter will be reset
            idleCounter = 0.0f;
        }

        // Reset playerMoving whenever no input is received
        playerMoving = false;

        // Reset playerRunning whenever no input is received
        playerRunning = false;

        // MovePlayer function call
        MovePlayer();
    }

    /* Function for detecting input and moving the Player */
    public void MovePlayer()
    {
        // Increase idleCounter while the Player isn't moving
        idleCounter += Time.deltaTime;

        // Get the input from the Horizontal and Vertical axis (WASD keys) and store it in moveInput
        moveInput = new Vector3(Input.GetAxisRaw("Horizontal"), 0.0f, Input.GetAxisRaw("Vertical"));
        
        // Check if any of the values in moveInput is not equal to 0
        if (moveInput != Vector3.zero)
        {
            // Reset idleCounter
            idleCounter = 0.0f;

            // Set targetRotation according to the value of moveInput
            targetRotation = Quaternion.LookRotation(moveInput);

            // set the GameObject's transform rotation according to targetRotation and rotationSpeed
            transform.eulerAngles = Vector3.up * Mathf.MoveTowardsAngle(transform.eulerAngles.y,
                                                                   targetRotation.eulerAngles.y,
                                                                   rotationSpeed * Time.deltaTime);

            // Acceleration
            currVelocityModifier = Vector3.MoveTowards(currVelocityModifier, moveInput, acceleration * Time.deltaTime);

            // Set the GameObject's velocity
            Vector3 motion = currVelocityModifier;

            // Set the GameObject's diagonal velocity when more than 1 key is pressed
            motion *= (Mathf.Abs(moveInput.x) == 1 && Mathf.Abs(moveInput.z) == 1) ? 0.7f : 1;

            // Check to see if the Run key has been pressed
            if (Input.GetButton("Run"))
            {
                // Multiply motion by runSpeed
                motion *= runSpeed;

                // Player is now running
                playerRunning = true;
            }
            else
            {
                // Multiply motion by walkSpeed
                motion *= walkSpeed;

                // Player is now walking
                playerRunning = false;
            }

            motion += Vector3.up * -8;

            // Moves the player
            cController.Move(motion * Time.deltaTime);

            // Player is moving
            playerMoving = true;
        }
    }
}
