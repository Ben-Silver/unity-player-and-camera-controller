﻿/*-------------------------------------------------*/
/*               CameraController.cs               */
/*              Created by Ben Silver              */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    /* Private variables */
    private Vector3 cameraTarget;           // Where the camera will be following
    private Transform target;               // The transform of the GameObject the camera will be following

    // Use this for initialization
    void Start()
    {
        // Finds the player, which will have the "Player" tag
        target = GameObject.FindGameObjectWithTag("Player").transform;
	}
	
	// Update is called once per frame
	void Update()
    {
        // Keeps the camera tracking the player's position
        cameraTarget = new Vector3(target.position.x, target.position.y + 6, target.position.z - 4.5f);

        // Keep the camera movements fluid when the player suddenly stops
        transform.position = Vector3.Lerp(transform.position, cameraTarget, Time.deltaTime * 8);
    }
}
